const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');

const app = express();
const defaultPort = 8000;

app.set('port', process.env.PORT || defaultPort);
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/public', express.static(`${__dirname}/public`));
app.use(express.static(`${__dirname}/public`));
app.use(favicon(`${__dirname}/img/favicon.ico`));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

require('./app/routes')(app, {});

app.listen(app.get('port'), () => {
    console.log(`server started on port ${app.get('port')}`);
});
