class DatabaseInteractor {
    /**
     * Query data from a specific database
     * @param httpUrl {string} url of the database
     * @param queryParams {JSON} query parameters
     * @return {Promise} the array of values resulting from query | the error
     */
    read (httpUrl, queryParams) {}

    /**
     * Insert data into a specific database
     * @param httpUrl {string} url of the database
     * @param queryParams {JSON} query parameters
     * @param dataList {JSON[]} data to write
     * @returns {Promise} the status code | the error
     */
    write (httpUrl, queryParams, dataList) {}
}

module.exports = DatabaseInteractor;
