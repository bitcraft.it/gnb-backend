const fs = require('fs');
const { JSDOM } = require('jsdom');
const { Network } = require('gnb-network/node');
const jsbayesviz = require('jsbayes-viz');
const NetworkHandler = require('./NetworkHandler');
const { getJSONNetworkFromFile } = require('./util/fs-util');
const InfluxInteractor = require('./InfluxInteractor');
const MySQLInteractor = require('./MySQLInteractor');

let instance;

class NetworksActivity {
    constructor () {
        this.activeNetworksGroup = new Map();
        this.databaseInteractors = new Map();
        this.initDatabaseInteractors();
    }

    /**
     * Initializes an instance for each database type
     */
    initDatabaseInteractors () {
        this.databaseInteractors.set('influxdb', new InfluxInteractor());
        this.databaseInteractors.set('mysql', new MySQLInteractor());
    }

    /**
     * Get the instance of a specific database interactor
     * @param databaseType {string} the database type
     * @returns {DatabaseInteractor} the database instance
     */
    getDatabaseInteractor (databaseType) {
        return this.databaseInteractors.get(databaseType);
    }

    /**
     * Start the network with the given id
     * @param networkID {string} the network id
     * @returns {Promise}
     */
    startNetworkWithID (networkID) {
        return new Promise((resolve, reject) => {
            getJSONNetworkFromFile(networkID).then(
                (data) => {
                    const network = Network.fromJSON(data);

                    if(network.getNodeLength() < 2) {
                        reject("The network must have at least 2 nodes.");
                    } else if(network.hasNodeIsolated()) {
                        reject("The network cannot have an isolated node.");
                    } else {
                        const networkHandler = new NetworkHandler(network, this);
                        networkHandler.startInterval();

                        this.activeNetworksGroup.set(networkHandler.network.id, networkHandler);
                        resolve("The network has been started!");
                    }
                },
                () => reject("There are no networks with this ID.")
            );
        });
    }

    /**
     * Stop the network with the given id
     * @param networkID {string} the network id
     * @returns {Promise}
     */
    stopNetworkWithID (networkID) {
        return new Promise((resolve, reject) => {
            if (this.isNetworkActive(networkID)) {
                const networkHandler = this.getNetworkWithID(networkID);

                networkHandler.stopInterval();
                this.activeNetworksGroup.delete(networkID);

                resolve("The network has been stopped!");
            } else {
                reject("No processes running for this network.");
            }
        });
    }

    /**
     * Get the NetworkHandler instance of the
     * given network id
     * @param networkID {string} the id of the network
     * @returns {NetworkHandler} the handler instance
     */
    getNetworkWithID (networkID) {
        return this.activeNetworksGroup.get(networkID);
    }

    /**
     * Check whether the network with the given id
     * has been started or not
     * @param networkID {string} the id of the network
     * @returns {boolean} true, if the network has been started, false otherwise
     */
    isNetworkActive (networkID) {
        return this.activeNetworksGroup.has(networkID);
    }

    getBayesianObjWithBasicInfoFromFile (dirPath, file) {
        const bayesianObjectWithBasicInfo = {};
        const fileContentInJSON = NetworksActivity.getContentInJSON(dirPath, file);
        bayesianObjectWithBasicInfo.id = fileContentInJSON.id;
        bayesianObjectWithBasicInfo.name = fileContentInJSON.name;
        bayesianObjectWithBasicInfo.active = this.isNetworkActive(fileContentInJSON.id);
        return bayesianObjectWithBasicInfo;
    }

    static getContentInJSON (dirPath, file) {
        const fileContent = fs.readFileSync(dirPath + file, 'utf8');
        return JSON.parse(fileContent);
    }

    static buildHTMLGraphFromNetwork (network, type) {
        const graph = jsbayesviz.fromGraph(network.graph);
        const dom = new JSDOM('<svg id="' + network.id + '_' + type + '"></svg>');
        jsbayesviz.draw({
            id: dom.window.document.querySelector('#' + network.id + '_' + type),
            graph,
            samples: 15000
        });

        network.nodes.forEach(node => {
            let elmId = dom.window.document.getElementById(node.id);
            let elmClass = elmId.getElementsByClassName("node-name")[0];
            elmClass.innerHTML = node.name;
        });

        return dom.window.document.getElementById(network.id + '_' + type).outerHTML;
    }

    static getInstance () {
        if (instance === undefined) {
            instance = new NetworksActivity();
        }

        return instance;
    }
}

exports.buildHTMLGraphFromNetwork = NetworksActivity.buildHTMLGraphFromNetwork;
exports.networksActivityInstance = NetworksActivity.getInstance();
