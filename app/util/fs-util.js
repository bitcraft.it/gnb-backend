const fs = require('fs');

/**
 * Get the file containing the definition
 * of the network with the given ID
 * @param networkID {string} the ID of the network
 * @returns {Promise} the JSON file | the error
 */
function getJSONNetworkFromFile (networkID) {
    return new Promise((resolve, reject) => {
        const filePath = `./public/network_${networkID}.json`;
        fs.readFile(filePath, (error, data) => {
            if (error) {
                reject("There are no networks with this ID.");
            } else {
                resolve(JSON.parse(data));
            }
        });
    });
}

/**
 * Save the file containing the definition
 * of the network with the given ID
 * @param networkID {string} the ID of the network
 * @param data {JSON} the network file
 * @returns {Promise} nothing | the error
 */
function writeNetworkFile (networkID, data) {
    return new Promise((resolve, reject) => {
        const filePath = `./public/network_${networkID}.json`;
        fs.writeFile(filePath, data, (error) => {
            if (error) {
                reject("Error while updating the network. Please try again later.");
            } else {
                resolve("The network has been saved!");
            }
        });
    });
}

/**
 * Delete the file containing the definition
 * of the network with the given ID
 * @param networkID {string} the ID of the network
 * @returns {Promise} nothing | the error
 */
function deleteNetworkFile (networkID) {
    return new Promise((resolve, reject) => {
        const filePath = `./public/network_${networkID}.json`;
        fs.unlink(filePath, (error) => {
            if (error) {
                reject("Error while deleting the network. Please try again later.");
            } else {
                resolve("The network has been deleted!");
            }
        });
    });
}

/**
 * Get a list of all network file
 * @returns {Promise} the network list | the error
 */
function listNetworkFiles () {
    return new Promise((resolve, reject) => {
        const dirPath = './public/';
        fs.readdir(dirPath, (error, files) => {
            if (error) {
                reject("Error while retrieving the network list. Please try again later.");
            } else {
                resolve(files);
            }
        });
    });
}

function isGitKeep (fileName) {
    return fileName === '.gitkeep';
}

/**
 * Get whether the file containing the definition
 * of the network with the given ID exists or not
 * @param networkID {string} the ID of the network
 * @returns {boolean} true, if the file exists, false otherwise
 */
function existsNetworkFile (networkID) {
    const filePath = `./public/network_${networkID}.json`;
    return fs.existsSync(filePath);
}

exports.getJSONNetworkFromFile = getJSONNetworkFromFile;
exports.writeNetworkFile = writeNetworkFile;
exports.deleteNetworkFile = deleteNetworkFile;
exports.listNetworkFiles = listNetworkFiles;
exports.existsNetworkFile = existsNetworkFile;
exports.isGitKeep = isGitKeep;
