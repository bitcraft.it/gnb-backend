const DatabaseInteractor = require('./DatabaseInteractor');

class MySQLInteractor extends DatabaseInteractor {
    /**
     * Query data from a specific mysql database
     * @param httpUrl {string} url of the database
     * @param queryParams {JSON} query parameters
     * @return {Promise} the array of values resulting from query | the error
     */
    read (httpUrl, queryParams) {
        // TODO implement
    }

    /**
     * Insert data into a specific mysql database
     * @param httpUrl {string} url of the database
     * @param queryParams {JSON} query parameters
     * @param dataList {JSON[]} data to write
     * @returns {Promise} the status code | the error
     */
    write (httpUrl, queryParams, dataList) {
        // TODO implement
    }
}

module.exports = MySQLInteractor;
