const express = require('express');
const { Network } = require('gnb-network/node');
const { networksActivityInstance, buildHTMLGraphFromNetwork } = require('../NetworksActivity');
const {
    getJSONNetworkFromFile, writeNetworkFile, deleteNetworkFile,
    listNetworkFiles, isGitKeep, existsNetworkFile
} = require('../util/fs-util');

const apiRoutes = express.Router({ mergeParams: true });

module.exports = (app) => {
    app.use('/api', apiRoutes);

    apiRoutes.get('/', (req, res) => {
        res.sendFile(`${__dirname}/index.html`);
    });

    apiRoutes.get('/retrieve/all', (req, res) => {
        const dirPath = './public/';
        const groupOfBayesianObjectsInFolderWithBasicInfo = [];

        listNetworkFiles().then(
            data => {
                data.forEach((file) => {
                    if (!isGitKeep(file)) {
                        groupOfBayesianObjectsInFolderWithBasicInfo.push(
                            networksActivityInstance.getBayesianObjWithBasicInfoFromFile(dirPath, file)
                        );
                    }
                });
                res.send(groupOfBayesianObjectsInFolderWithBasicInfo);
            },
            error => res.status(500).send(error)
        );
    });

    apiRoutes.get('/retrieve/:id', (req, res) => {
        const idOfNetworkToRetrieve = req.params.id;

        getJSONNetworkFromFile(idOfNetworkToRetrieve).then(
            data => res.send(data),
            error => res.status(400).send(error)
        );
    });

    apiRoutes.get('/start/:id', (req, res) => {
        const idOfNetworkToStart = req.params.id;

        if (networksActivityInstance.isNetworkActive(idOfNetworkToStart)) {
            res.status(400).send("Process already started for this network.");
        } else {
            networksActivityInstance.startNetworkWithID(idOfNetworkToStart).then(
                data => res.send(data),
                error => res.status(400).send(error)
            );
        }
    });

    apiRoutes.get('/stop/:id', (req, res) => {
        const idOfNetworkToStop = req.params.id;

        networksActivityInstance.stopNetworkWithID(idOfNetworkToStop).then(
            data => res.send(data),
            error => res.status(400).send(error)
        );
    });

    apiRoutes.post('/save', (req, res) => {
        const idOfNetworkToSave = req.body.id;

        if(existsNetworkFile(idOfNetworkToSave)) {
            res.status(400).send("The network's ID already exists.");
        } else {
            const data = JSON.stringify(req.body);
            writeNetworkFile(idOfNetworkToSave, data).then(
                data => res.send(data),
                error => res.status(500).send(error)
            );
        }
    });

    apiRoutes.get('/delete/:id', (req, res) => {
        const idOfNetworkToDelete = req.params.id;

        if (networksActivityInstance.isNetworkActive(idOfNetworkToDelete)) {
            res.status(400).send("The network is currently active. Please stop it before deleting.");
        } else if (existsNetworkFile(idOfNetworkToDelete)) {
            deleteNetworkFile(idOfNetworkToDelete).then(
                data => res.send(data),
                error => res.status(500).send(error)
            );
        } else {
            res.status(400).send("There are no networks with this ID.");
        }
    });

    apiRoutes.post('/update/:id', (req, res) => {
        const idOfNetworkToUpdate = req.params.id;

        if (networksActivityInstance.isNetworkActive(idOfNetworkToUpdate)) {
            res.status(400).send("The network is currently active. Please stop it before updating.");
        } else if (existsNetworkFile(idOfNetworkToUpdate)) {
            const data = JSON.stringify(req.body);
            writeNetworkFile(idOfNetworkToUpdate, data).then(
                data => res.send(data),
                error => res.status(500).send(error)
            );
        } else {
            res.status(400).send("There are no networks with this ID.")
        }
    });

    apiRoutes.get('/static-graph/:id', (req, res) => {
        const idOfStaticGraph = req.params.id;

        getJSONNetworkFromFile(idOfStaticGraph).then(
            (jsonNetwork) => {
                const network = Network.fromJSON(jsonNetwork);
                network.graph.sample(100000);
                const staticGraphDOMElement = buildHTMLGraphFromNetwork(network, "STATIC");
                res.send(staticGraphDOMElement);
            },
            error => res.status(400).send(error)
        );
    });

    apiRoutes.get('/dynamic-graph/:id', (req, res) => {
        const idOfDynamicGraph = req.params.id;

        if (networksActivityInstance.isNetworkActive(idOfDynamicGraph)) {
            const dynamicGraphDOMElement = buildHTMLGraphFromNetwork(
                networksActivityInstance.getNetworkWithID(idOfDynamicGraph).network, "DYNAMIC"
            );
            res.send(dynamicGraphDOMElement);
        } else {
            res.status(400).send("No processes running for this network.");
        }
    });
};
