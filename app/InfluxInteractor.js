const request = require('request');
const DatabaseInteractor = require('./DatabaseInteractor');

class InfluxInteractor extends DatabaseInteractor {
    /**
     * Query data from a specific influx database
     * @param httpUrl {string} url of the database
     * @param queryParams {JSON} query parameters
     * @return {Promise} the array of values resulting from query | the error
     */
    read (httpUrl, queryParams) {
        const query = `/query?u=${queryParams.user}&p=${queryParams.password}&db=${queryParams.database}&q=${queryParams.query}`;

        return new Promise((resolve, reject) => {
            request.get(`${httpUrl}${query}`)
                .on('error', error => reject(error))
                .on('data', data => resolve(JSON.parse(data).results[0].series[0].values));
        });
    }

    /**
     * Insert data into a specific influx database
     * @param httpUrl {string} url of the database
     * @param queryParams {JSON} query parameters
     * @param dataList {JSON[]} data to write
     * @returns {Promise} the status code | the error
     */
    write (httpUrl, queryParams, dataList) {
        const query = `/write?u=${queryParams.user}&p=${queryParams.password}&db=${queryParams.database}&precision=s`;
        let dataToSend = '';

        dataList.forEach((data) => {
            dataToSend += `${queryParams.tableName} ${data.key}=${data.value}\n`;
        });

        return new Promise((resolve, reject) => {
            request.post(`${httpUrl}${query}`, { form: dataToSend })
                .on('error', error => reject(error))
                .on('response', response => resolve(response.statusCode));
        });
    }
}

module.exports = InfluxInteractor;
