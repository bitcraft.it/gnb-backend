class NetworkHandler {
    /**
     * Build a NetworkHandler instance
     * @param network {Network} the network object
     * @param networksActivityInstance {NetworksActivity} the NetworksActivity instance
     */
    constructor (network, networksActivityInstance) {
        this.network = network;
        this.networksActivityInstance = networksActivityInstance;
    }

    startInterval () {
        this.clockID = setInterval(() => this.update(), this.network.refreshTime);
    }

    stopInterval () {
        if (this.clockID !== undefined) {
            clearInterval(this.clockID);
        }
    }

    update () {
        const promises = [];
        this.network.nodes.forEach((node) => {
            if (node.hasSensor()) {
                const p = this.getSensorValue(node.sensor).then(
                    data => this.network.graph.observe(node.id, node.getState(data)),
                    () => this.networksActivityInstance.stopNetworkWithID(this.network.id)
                );
                promises.push(p);
            }
        });
        Promise.all(promises).then(
            () => this.saveNetworkData(),
            () => this.networksActivityInstance.stopNetworkWithID(this.network.id)
        );
    }

    /**
     * Get the current value of the sensor
     * connected by the node
     * @param sensor {Sensor} the sensor
     * @returns {Promise} the value of the sensor latest registered | the error
     */
    getSensorValue (sensor) {
        return new Promise((resolve, reject) => {
            const [httpUrl, queryParams] = NetworkHandler.buildReadRequest(sensor);
            const interactor =
                this.networksActivityInstance.getDatabaseInteractor(sensor.databaseSensorType);
            interactor.read(httpUrl, queryParams).then(
                data => resolve(data[0][1]),
                error => reject(error)
            );
        });
    }

    static buildReadRequest (sensor) {
        const httpUrl = sensor.databaseSensorUrl;
        const queryParams = {
            user: sensor.databaseSensorUser,
            password: sensor.databaseSensorPassword,
            database: sensor.databaseSensorName,
            query: `SELECT ${sensor.databaseSensorColumn} FROM ${sensor.databaseSensorTable} ORDER BY time DESC LIMIT 1`
        };
        return [httpUrl, queryParams];
    }

    /**
     * Save the network parameters into the database
     */
    saveNetworkData () {
        this.network.graph.sample(20000).then(() => {
            const [httpUrl, queryParams, dataToSend] = this.buildWriteRequest();
            const interactor =
                this.networksActivityInstance.getDatabaseInteractor(this.network.databaseWriteType);
            interactor.write(httpUrl, queryParams, dataToSend).then(
                () => {},
                () => this.networksActivityInstance.stopNetworkWithID(this.network.id)
            );
        });
    }

    buildWriteRequest () {
        const httpUrl = this.network.databaseWriteUrl;
        const queryParams = {
            user: this.network.databaseWriteUser,
            password: this.network.databaseWritePassword,
            database: this.network.databaseWriteName,
            tableName: `${this.network.name.replace(/ /g, '_')}[${this.network.id}]`
        };
        const dataToSend = this.composeWriteDataBody();
        return [httpUrl, queryParams, dataToSend];
    }

    composeWriteDataBody () {
        const dataList = [];
        this.network.nodes.forEach((node) => {
            const probabilities = this.network.graph.node(node.id).probs();
            node.stateList.forEach((state, index) => {
                dataList.push({
                    key: `${node.name.replace(/ /g, '_')}_${state.name.replace(/ /g, '_')}`,
                    value: `${probabilities[index]}`
                });
            });
        });

        return dataList;
    }
}

module.exports = NetworkHandler;
